/*
  Web Projects via vue
*/
Vue.component('clock', {
  mounted: function() {
    const secondHand = document.querySelector('.second-hand');
    const minuteHand = document.querySelector('.min-hand');
    const hourHand = document.querySelector('.hour-hand');

    function setDate() {
      const now = new Date;
      const seconds = now.getSeconds();
      const secondsDegrees = ((seconds / 60) * 360) + 90;

      const minutes = now.getMinutes();
      const minutesDegrees = ((minutes / 60) * 360) + 90;

      const hours = now.getHours();
      const hoursDegrees = ((hours / 12) * 360) + 90;

      secondHand.style.transform = `rotate(${secondsDegrees}deg)`;
      minuteHand.style.transform = `rotate(${minutesDegrees}deg)`;
      hourHand.style.transform = `rotate(${hoursDegrees}deg)`;
    }

    setInterval(setDate, 1000);
  },
  template: `
  <div class='clock'>
    <div class='project-description'>
      <h1>Analog Clock</h1>
      <p>Check the time</p>
    </div>

    <div class='project-example'>
      <div class="clock-face">
        <div class="hand hour-hand"></div>
        <div class="hand min-hand"></div>
        <div class="hand second-hand"></div>
      </div>
    </div>

    <div class='code-snippet shadow'>
      <code>
        <p>
         <span class='var-declaration'>const </span><span class='var-name'>now</span><span class='operator'> = </span><span class='method'>new</span><span class='object'> Date</span><span class='code-text'>;</span>
        </p>
        <p>
         <span class='var-declaration'>const </span><span class='var-name'>seconds</span><span class='operator'> = </span><span class='var-refer'>now</span><span class='code-text'>.</span><span class='method'>getSeconds()</span><span class='code-text'>;</span>
        </p>
        <p>
         <span class='var-declaration'>const </span><span class='var-name'>secondsDegrees </span><span class='operator'> = </span><span class='code-text'>((</span><span class='var-refer'>seconds </span><span class='operator'>/ </span><span class='code-text'>60) </span><span class='operator'>* </span><span class='code-text'>360) </span><span class='operator'>+ </span><span class='code-text'>90;</span>
        </p>

        <br>

        <p>
         <span class='var-declaration'>const </span><span class='var-name'>minutes</span><span class='operator'> = </span><span class='var-refer'>now</span><span class='code-text'>.</span><span class='method'>getMinutes()</span><span class='code-text'>;</span>
        </p>
        <p>
         <span class='var-declaration'>const </span><span class='var-name'>minutesDegrees </span><span class='operator'> = </span><span class='code-text'>((</span><span class='var-refer'>minutes </span><span class='operator'>/ </span><span class='code-text'>60) </span><span class='operator'>* </span><span class='code-text'>360) </span><span class='operator'>+ </span><span class='code-text'>90;</span>
        </p>

        <br>

        <p>
         <span class='var-declaration'>const </span><span class='var-name'>hours</span><span class='operator'> = </span><span class='var-refer'>now</span><span class='code-text'>.</span><span class='method'>getHours()</span><span class='code-text'>;</span>
        </p>
        <p>
         <span class='var-declaration'>const </span><span class='var-name'>hoursDegrees </span><span class='operator'> = </span><span class='code-text'>((</span><span class='var-refer'>hours </span><span class='operator'>/ </span><span class='code-text'>60) </span><span class='operator'>* </span><span class='code-text'>360) </span><span class='operator'>+ </span><span class='code-text'>90;</span>
        </p>
      </code>
    </div>


  </div>
  `,
})
Vue.component('calculator', {
  props: {
    operations: Array
  },

  data: function() {
    return {
      answer: '',
      num1: '',
      num2: '',
      operator: ''
    }
  },

  template: `
    <div class='calculator'>
      <div class='project-description'>
        <h1>Calculator</h1>
        <p>Fully responsive</p>
      </div>

      <div class='project-example'>
            <p>
                Number:
                <input class="input" type="number" placeholder="..." v-model='num1'>
            </p>
            <p>
                Operator:
                <input class="input" type="text" placeholder="+, -, /, *" v-model='operator'>
            </p>
            <p>
                Number:
                <input class="input" type="number" placeholder="..." v-model='num2'>
            </p>

    	       <h2>{{answer}}</h2>
      </div>

      <div class='code-snippet shadow'>
          <code>
              <p>
                  <span class='code-text'>A calculator must have a collection of operations it is able to perform. Although I've only included the very basic of mathematical computations, I could increase the functionality of the calculator at any time by simply adding a formula to this array.</span>
              </p>
              <br>
              <p>
                  <span class='var-declaration'>operations: [</span><br>
              </p>
              <p>
                  <span class='indented code-text'>{ '</span><span class='string'>value</span><span class='code-text'>'</span><span class='operator'> : </span><span class='code-text'>'</span><span class='string'>+</span><span class='code-text'>', '</span><span class='var-declaration'>do</span><span class='code-text'>'</span><span class='operator'> : </span><span class='function'>function</span><span class='code-text'>(a, b) { </span><span class='method'>return </span><span class='code-text'>a + b</span><span class='operator'>; </span><span class='code-text'>} },</span>
              </p>
              <p>
                  <span class='indented code-text'>{ '</span><span class='string'>value</span><span class='code-text'>'</span><span class='operator'> : </span><span class='code-text'>'</span><span class='string'>-</span><span class='code-text'>', '</span><span class='var-declaration'>do</span><span class='code-text'>'</span><span class='operator'> : </span><span class='function'>function</span><span class='code-text'>(a, b) { </span><span class='method'>return </span><span class='code-text'>a - b</span><span class='operator'>; </span><span class='code-text'>} },</span>
              </p>
              <p>
                  <span class='indented code-text'>{ '</span><span class='string'>value</span><span class='code-text'>'</span><span class='operator'> : </span><span class='code-text'>'</span><span class='string'>/</span><span class='code-text'>', '</span><span class='var-declaration'>do</span><span class='code-text'>'</span><span class='operator'> : </span><span class='function'>function</span><span class='code-text'>(a, b) { </span><span class='method'>return </span><span class='code-text'>a / b</span><span class='operator'>; </span><span class='code-text'>} },</span>
              </p>
              <p>
                  <span class='indented code-text'>{ '</span><span class='string'>value</span><span class='code-text'>'</span><span class='operator'> : </span><span class='code-text'>'</span><span class='string'>*</span><span class='code-text'>', '</span><span class='var-declaration'>do</span><span class='code-text'>'</span><span class='operator'> : </span><span class='function'>function</span><span class='code-text'>(a, b) { </span><span class='method'>return </span><span class='code-text'>a * b</span><span class='operator'>; </span><span class='code-text'>} },</span>
              </p>

              <br>
          </code>
      </div>
    </div>
  `,

  methods: {
    currentOperation: function() {
      for (let i = 0; i < this.operations.length; i++) {
        if (this.operations[i].value === this.operator) return i;
      }
    },

    result: function() {
      let reactiveNum1 = parseInt(this.num1);
      let reactiveNum2 = parseInt(this.num2);
      this.answer = this.operations[this.currentOperation()].do(reactiveNum1, reactiveNum2);
    }
  },

  watch: {
    num1: function() {
      if (this.num1 !== '' && this.num2 !== '') this.result();
    },
    num2: function() {
      if (this.num1 !== '' && this.num2 !== '') this.result();
    },
    operator: function() {
      if (this.num1 !== '' && this.num2 !== '' && this.operator !== '') this.result();
    }
  }
})
Vue.component('drumkit', {
  mounted: function() {
    const keys = document.querySelectorAll('.key');
    keys.forEach(key => key.addEventListener('transitionend', this.removeTransition));
    window.addEventListener('keydown', this.playSound);
  },
  methods: {
    playSound: function(e) {
      const audio = document.querySelector(`audio[data-key="${e.keyCode}"]`);
      const key = document.querySelector(`.key[data-key='${e.keyCode}']`)
      if (!audio) return; // stop function from running off together
      audio.currentTime = 0; // rewind to start for repitive key presses
      audio.play();
      key.classList.add('playing');
    },
    removeTransition: function(e) {
      if (e.propertyName !== 'transform') return; // skip if it's not a transform
      e.target.classList.remove('playing');
    }
  },
  template: `
  <div class='drumkit'>
    <div class='project-description'>
      <h1>Musical Keyboard</h1>
      <p>Press a key</p>
    </div>

    <div class='project-example'>
      <div class="keys">
        <div data-key="65" class="key">
          <kbd>A</kbd>
          <span class="sound">clap</span>
        </div>
        <div data-key="83" class="key">
          <kbd>S</kbd>
          <span class="sound">hihat</span>
        </div>
        <div data-key="68" class="key">
          <kbd>D</kbd>
          <span class="sound">kick</span>
        </div>
        <div data-key="70" class="key">
          <kbd>F</kbd>
          <span class="sound">snare</span>
        </div>
        <div data-key="71" class="key">
          <kbd>G</kbd>
          <span class="sound">boom</span>
        </div>
      </div>

      <audio data-key="65" src="sounds/clap.wav"></audio>
      <audio data-key="83" src="sounds/hihat.wav"></audio>
      <audio data-key="68" src="sounds/kick.wav"></audio>
      <audio data-key="70" src="sounds/snare.wav"></audio>
      <audio data-key="71" src="sounds/boom.wav"></audio>
    </div>


    <div class='code-snippet'>
      <code>
        <p><span class='code-text'>
        This bit was something that got me for a second. I kept thinking I was missing something with my transitions, but once I remembered audio functionality is JavaScript's responsibility, I narrowed it down to the sound not repeating as fast as you could press a specific key. Your solution:
        </span></p>

        <br>

        <p>
          <span class='object'>if </span><span class='code-text'>(<span class='operator'>!</span></span><span class='var-refer'>audio</span><span class='code-text'>) </span><span class='object'>return</span><span class='code-text'>;</span>
        </p>
        <p>
          <span class='var-refer'>audio</span><span class='code-text'>.</span><span class='method'>currentTime </span><span class='operator'>=</span> <span class='code-text'>0;</span>
        </p>
        <p>
          <span class='var-refer'>audio</span><span class='code-text'>.</span><span class='method'>play()</span><span class='code-text'>;</span>
        </p>
      </code>
    </div>
  </div>
  `
})
Vue.component('flexpanel', {
  mounted: function() {
    const panels = document.querySelectorAll('.flexpanel-panel');

    function toggleOpen() {
      console.log(this);
      this.classList.toggle('open');
    }

    function toggleOpenActive(e) {
      console.log('openactive triggered');
      if (e.propertyName.includes('flex')) {
        this.classList.toggle('open-active');
      }
    }

    panels.forEach(panel => panel.addEventListener('click', toggleOpen));
    panels.forEach(panel => panel.addEventListener('transitionend', toggleOpenActive));
  },
  template: `
  <div class='flexpanel'>
    <div class='project-description'>
      <h1>Adjust on Click</h1>
      <p>I hadn't worked with CSS transitions prior to this project, but as you can see, they really add to the user experience.</p>
    </div>

    <div class='project-example'>
      <div class='flexpanel'>
        <div class="flexpanel-panels">
          <div class="flexpanel-panel flexpanel-panel1">
            <p>Hey</p>
            <p>Let's</p>
            <p>Dance</p>
          </div>
          <div class="flexpanel-panel flexpanel-panel2">
            <p>Give</p>
            <p>Take</p>
            <p>Receive</p>
          </div>
          <div class="flexpanel-panel flexpanel-panel3">
            <p>Experience</p>
            <p>It</p>
            <p>Today</p>
          </div>
          <div class="flexpanel-panel flexpanel-panel4">
            <p>Give</p>
            <p>All</p>
            <p>You can</p>
          </div>
          <div class="flexpanel-panel flexpanel-panel5">
            <p>Life</p>
            <p>In</p>
            <p>Motion</p>
          </div>
        </div>
      </div>
    </div>

    <div class='code-snippet shadow'>
      <code>
        <p>
          <span class='code-text'>transition:</span><br>
          <span class='code-text indented'>font-size</span> <span class='object'>0.7s</span> <span class='code-text'>cubic-bezier(</span><span class='object'>0.61</span><span class='code-text'>,</span><span class='object'>-0.19</span><span class='code-text'>,</span> <span class='object indented'>0.7</span><span class='code-text'>,</span><span class='object'>-0.11</span><span class='code-text'>),</span><br>
          <span class='object indented'>flex< 0.7</span> <span class='code-text'>cubic-bezier(</span><span class='object'>0.61</span><span class='code-text'>,</span><span class='object'>-0.19</span><span class='code-text'>,</span> <span class='object indented'>0.7</span><span class='code-text'>,</span><span class='object'>-0.11</span><span class='code-text'>),</span><br>
          <span class='code-text indented'>background</span> <span class='object'>0.2s</span><span class='code-text'>;</span>
        </p>
      </code>
    </div>
  </div>
  `,
})
Vue.component('typeahead', {
  mounted: function() {
    const endpoint = 'https://gist.githubusercontent.com/Miserlou/c5cd8364bf9b2420bb29/raw/2bf258763cdddd704f8ffd3ea9a3e81d25e2c6f6/cities.json';

    const cities = [];

    fetch(endpoint).then(blob => blob.json()).then(data => cities.push(...data));

    function findMatches(wordToMatch, cities) {
      return cities.filter(place => {
        const regex = new RegExp(wordToMatch, 'gi');
        return place.city.match(regex) || place.state.match(regex);
      })
    }

    function numberWithCommas(x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }

    function clearDisplay () {
      let clear = `<li>Filter for a city</li><li>or a state</li>`;
      suggestions.innerHTML = clear;
    }

    /* IDEA TO DISPLAY ONLY TOP 3 RESULTS

    1.Sort matches by largest population first
    2.Filter sorted matches and display top 3

    */

    function displayMatches() {
      const matchArray = findMatches(this.value, cities);
      const html = matchArray.map(place => {
        const regex = new RegExp(this.value, 'gi');
        const cityName = place.city.replace(regex, `<span class='typeahead-hl'>${this.value}</span>`);
        const stateName = place.state.replace(regex, `<span class='typeahead-hl'>${this.value}</span>`);
        return `
          <li>
            <span class='typeahead-name'>${cityName}, ${stateName}</span>
            <span class='typeahead-population'>${numberWithCommas(place.population)}</span>
          </li>
        `;
      }).join('');

      if (this.value === '') {
        clearDisplay();
      } else suggestions.innerHTML = html;
    }

    const searchInput = document.querySelector('.typeahead-search');
    const suggestions = document.querySelector('.typeahead-suggestions');

    searchInput.addEventListener('change', displayMatches);
    searchInput.addEventListener('keyup', displayMatches);

  },
  data: function() {
    return {
      span: "<span class='typeahead-hl'>",
      endSpan: "</span>",
      li: "<li>",
      endLi: "</li>",
      nameSpan: "<span class='typeahead-name'>",
      popSpan: "<span class='typeahead-population'>",
      bracketOpen: "{",
      paranthOpen: "("
    }
  },
  template: `
  <div class='typeahead'>
    <div class='project-description'>
      <h1>Population API</h1>
      <p>Check out your home's population.</p>
    </div>

    <div class='project-example'>
      <div class='typeahead'>
        <form class="typeahead-search-form">
          <input type="text" class="typeahead-search" placeholder="City">
          <ul class="typeahead-suggestions">
            <li>Filter for a city</li>
            <li>or a state</li>
          </ul>
        </form>
      </div>
    </div>

    <div class='code-snippet shadow'>
      <code>
        <p class='code-text'> Regular expressions are a very powerful tool, but every now and then you run into issues with them being registered as a string instead of an expression.
        </p>
        <br>
        <p>
          <span class='var-declaration'>const </span><span class='var-name'>html </span><span class='code-text'>= </span><span class='var-refer'>matchArray</span><span class='code-text'>.</span><span class='method'>map</span><span class='code-text'>(place </span><span class='operator'>=> </span><span class='code-text'>{</span>
        </p>
        <p>
          <span class='var-declaration indented'>const </span><span class='var-name'>regex </span><span class='code-text'>= new </span><span class='object'>RegExp</span><span class='code-text'>(</span><span class='var-refer'>this</span><span class='code-text'>.</span><span class='property'>value</span><span class='code-text'>, </span><span class='string'>'gi'</span><span class='code-text'>);</span>
        </p>
        <p>
          <span class='var-declaration indented'>const </span><span class='var-name'>cityName </span><span class='code-text'>= new </span><span class='var-refer'>place</span><span class='code-text'>.</span><span class='property'>city</span><span class='code-text'>.</span><span class='var-refer'>regex</span><span class='code-text'>, </span><span class='string'>\`{{span}}\`</span><span class='template-string'>\${</span><span class='var-refer'>this</span><span class='string'>.</span><span class='property'>value</span><span class='template-string'>}</span><span class='string'>\`</span><span class='method'>)</span><span class='code-text'>;</span>
        </p>
        <p>
          <span class='var-declaration indented'>const </span><span class='var-name'>stateName </span><span class='code-text'>= new </span><span class='var-refer'>place</span><span class='code-text'>.</span><span class='property'>state</span><span class='code-text'>.</span><span class='var-refer'>regex</span><span class='code-text'>, </span><span class='string'>\`{{span}}\`</span><span class='template-string'>\${</span><span class='var-refer'>this</span><span class='string'>.</span><span class='property'>value</span><span class='template-string'>}</span><span class='string'>\`</span><span class='method'>)</span><span class='code-text'>;</span>
        </p>
        <p>
          <span class='object indented'>return </span><br>
          <span class='string indented-2'>\`{{li}}</span><br>
          <span class='string indented-3'>{{nameSpan}}</span><span class='template-string'>\${</span><span class='var-refer'>cityName</span><span class='template-string'>}</span><span class='string'>, </span><span class='template-string'>\${</span><span class='var-refer'>stateName</span><span class='template-string'>}</span><span class='string'>{{endSpan}}</span><br>

          <span class='string indented-3'>{{popSpan}}</span>
          <span class='template-string'>\${{bracketOpen}}</span>
          <span class='function'>numberWithCommas{{paranthOpen}}</span>
          <span class='var-refer'>place</span>
          <span class='code-text'>.</span>
          <span class='property'>population</span>
          <span class='function'>)</span>
          <span class='template-string'>}</span>
          <span class='string'>{{endSpan}}</span><br>

          <span class='string indented-2'>{{endLi}}\`</span>
        </p>
      </code>
    </div>
  </div>
  `
})


var app = new Vue({
  el: '#app',
  data: {
    operations: [
      { 'value':'+', 'do':function(a, b) { return a + b; } },
      { 'value':'-', 'do':function(a, b) { return a - b; } },
      { 'value':'/', 'do':function(a, b) { return a / b; } },
      { 'value':'*', 'do':function(a, b) { return a * b; } }
    ],
    clock: false,
    calculator: false,
    drumkit: false,
    panels: false,
    typeAhead: true
  },
  mounted: function() {
    const projects = Array.from(document.querySelectorAll('.project'));

    function toggleAllClosed() {
      projects.forEach(project => project.classList.remove('open'));
    }

    function toggleOpen() {
      toggleAllClosed();
      this.classList.toggle('open');
    }

    projects.forEach(project => project.addEventListener('click', toggleOpen));
  },
  methods: {
    allFalse: function() {
      this.clock = false;
      this.calculator = false;
      this.drumkit = false;
      this.panels = false;
      this.typeAhead = false;
    },
    clockTrue: function() {
      this.allFalse();
      this.clock = true;
    },
    calculatorTrue: function() {
      this.allFalse();
      this.calculator = true;
    },
    drumkitTrue: function() {
      this.allFalse();
      this.drumkit = true;
    },
    panelsTrue: function() {
      this.allFalse();
      this.panels = true;
    },
    typeAheadTrue: function() {
      this.allFalse();
      this.typeAhead = true;
    }
  }
})
