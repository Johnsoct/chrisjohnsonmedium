BRIEF
PrJ: Portfolio - Convey a complete 'start-from-scratch' service
TA: Creatives, non-commercial-minded, out-of-the-box thinkers
GO: CRO Funnels
    1. Contact: View [project] -> Contact -> $$$
                  |-> Share ->
          *IDEA: Contact button can be full width with only a single message: 'X' - Super readable and impossible to miss
    2. Blog Newsletter: read blog -> Sign up -> Increase Client Potential
          *IDEA: Blog doesn't have to live on site - can simply link to blog home, such as MEDIUM
          *IDEA: Lightly designed HTML pages would make a great platform for individual posts to sit on
TS: Mobile-responsive: iphone 5 and on, Android Google Chrome, iPads
    Domains: chrisjohnsonmedium.com -or- johnsoct.com
    Newsletter: Mailchimp
    Analytics: Google
    CMS(blog): Pagekit
CI: Photography projects, video projects, web projects
RE: ....
PrT: Development: January 31st, 2017
     Testing and Launch: February 3rd, 2017
PrB: Hosting

PLANNING
RC: How should the site feel to others: energetic, well thought out, content/client oriented, out-of-the-box
    How should the site feel to me: free-flowing, fresh, demonstrative of my business and coding capabilities
BR: Focus is: Business Web Development: A complete service, "I take information, business objectives, and your ideas and actually make something that performs for your needs."
    This includes: Business and industry research (i.e. business objectives and goals, competition, feature integration), design and information structure using CRO Funnels, SEO, and research information, code development, variety of testing, launch, and maintenance, which includes A/B testing, analytic reporting/testing/changes, and future integrations.

    Photo/Video: Where does it fit in?
      "Coming from a professional background in photo and video, Chris..."
      May drop videography from site, as:
        1. Too expensive to keep up with industry
        2. Too timely to do alongside web development
      Focus on Photography = display my work second to web development
IA: Information in order of Level of importance, not order of appearance (will follow closely with some integration):
  1. Hi, I'm [Drumkit]
  2. Complete Service
  3. Web Portfolio
  4. About + Contact
  5. Blog + Newsletter
  6. Photo portfolio + Vlog section

  *No skill sections as I'll include keywords/phrases/sentences hinting at extra skills that I have
  Skills:
    1. SEO
    2. CRO
    3. Business Basics
    4. System Analysis
    5. Analytics

DESIGN
CS: #14CCC5, #3D9995, #00FF6A, #FF407A, #CC14A8
Lo: none
Ar: none
VE: Tested in /element-design/elementdesigns.html
Ty: Header: Open Sans 700; Paragraph: Raleway 300
RM: If any, already within project folders.
UX: Will text after construction.
CdC: Using Bulma, which is responsive on 4 desktop breakpoints and 7 mobile. Any issues will be handled manually.
PT: No prototypes will be completed.

DEVELOPMENT
Fr: CSS: Bulma; JS: Vue; Blog: PageKit (maybe)
Db: None
